<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Newsletter;
use App\TokenGenerator;
use App\Mail\NewsletterMail;
use Auth;
use Toastr; 



class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
    }


    
    public function index()
    {


        return view('frontend.pages.home.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function sendEmail(Request $request){

        $email = $request->input('email');
        $name = $request->input('name');
        $msg = $request->input('msg');
 
       
        Mail::send('emails.contacts', 
            [   'email' => $email, 
                'name' => $name, 
                'msg' => $msg
            ], 
            function ($message){

        });

        Toastr::success('Email sent success', 'Contacto', [   "positionClass" => "toast-top-center"]);

        return Redirect::back();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $newsletter = new Newsletter();
        $newsletter->email =  $_POST['email'];
        $newsletter->token =  new TokenGenerator(); 
        $newsletter->token = $newsletter->token->uuid;

        $content = [
        	'token' => $newsletter->token,
        ];


        if(Newsletter::where('email', '=', $newsletter->email )->count() > 0){
            Toastr::error('Déjà inscrit dans la newsletter', 'Inscription à la Newsletter', [   "positionClass" => "toast-top-center"]);

        }else{
            //save to DB & send email
            $newsletter->save();

            Mail::to('r.guerreiro@comup.pt')->send(new NewsletterMail($content));

            Toastr::success('Abonné avec succès à notre newsletter', 'Inscription à la Newsletter', [   "positionClass" => "toast-top-center"]);
        }

        return Redirect::back();
    }


    public function confirmNewsletter($token){
        //search for token on DB
        //if token exists, change row's state to active

        DB::table('newsletter')->where('token', $token)->update(['state' => 1]);

        Toastr::success('Newsletter subscription confirmed with success', 'Inscription à la Newsletter', [   "positionClass" => "toast-top-center"]);

        $contacts = DB::table('contacts')->get();
        $cars = DB::table('cars')->limit(6)->get();

        return redirect()->to('/');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function searchData(Request $request) {
        $term = $request->get('term');
        $data = DB::table('cars')->where("name","LIKE","%$term%")->select('id as id', 'name as name','subCategoriesId as subCategoriesId', 'categoriesId as categoriesId', 'brandId as brandId')->get();
       
        foreach($data as $car){
            $brand = DB::table('brands')->where('id','=',$car->brandId)->select('name as name')->get();
            $subcategory = DB::table('subcategories')->where('id','=',$car->subCategoriesId)->select('name as name')->get();
            $category = DB::table('categories')->where('id','=',$car->categoriesId)->select('name as name')->get(); 

            $car->value = strtoupper( $category[0]->name.' - '.$subcategory[0]->name.' '.$brand[0]->name. ' '. $car->name);
        }
      
        return response()->json($data);
 
       // Something to note here : autocomplete takes value as your data so your title should be displayed as value as i did in my query else data will not be displayed
    }

    public function getPortfolio(){
        $portfolio = DB::table('portfolio')->where('title','=',$_GET['name'])->get()->first();

        $files = array();

        $images = File::allFiles(public_path().'/images/portfolio'.'/'.$portfolio->id.'/slider');

        foreach($images as $image){
            $mainImage = $image->getfilename();
            $asset = asset('/images/portfolio/'.$portfolio->id.'/slider'.'/'.$mainImage);
            $files[] = $asset;
        }

        return compact('files');    
    }
}
