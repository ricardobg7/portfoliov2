@extends('frontend.main')

@section('content')

<section id="intro">
  <div class="section-title">
    <h1> intro </h1>
  </div>
  <div class="section-content"> 
    <p> This is a simple demonstration of organizing different elements without bootstrap so that when a browser is resized the elements adjust accordingly. The goal is obviously to be completely functional with as simple coding as possible. This means avoiding media queries when possible etc. Maybe this is common knowledge for most but as I'm growing and learning, I haven't found a comprehensive tutorial on the web with this stuff. SO..here's my two cents.</p>
  </div>
</section>
<section id="skills">
  <div class="section-title">
    <h1> Skill </h1>
    <div class="services-grid">
      <div class="service service1">
        <i class="ti-bar-chart"></i>
        <h4>Frontend</h4>
        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <ul> 
          <li>
            <i class="fas fa-caret-right"> </i> teste
          </li>
        </ul>
        <a class="btn-draw">Read More  <i class="fas fa-chevron-right"></i></a>
      </div>
  
      <div class="service service2">
        <i class="ti-light-bulb"></i>
        <h4>Backend</h4>
        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <a href="#" class="btn-dark">Read More  <i class="fas fa-chevron-right"></i></a>
      </div>
  
    </div>
  </div>

</section>
<section id="portfolio">
  <div class="section-title">
      <h1> portfolio </h1>
  </div>
  <div class="section-content"> 
    <p> This is a simple demonstration of organizing different elements without bootstrap so that when a browser is resized the elements adjust accordingly. The goal is obviously to be completely functional with as simple coding as possible. This means avoiding media queries when possible etc. Maybe this is common knowledge for most but as I'm growing and learning, I haven't found a comprehensive tutorial on the web with this stuff. SO..here's my two cents.</p>
  </div>
</section>

<section id="contacts">
  <div class="section-title">
    <h1> Contacts </h1>
  </div>
  <div class="section-content"> 
    <p> This is a simple demonstration of organizing different elements without bootstrap so that when a browser is resized the elements adjust accordingly. The goal is obviously to be completely functional with as simple coding as possible. This means avoiding media queries when possible etc. Maybe this is common knowledge for most but as I'm growing and learning, I haven't found a comprehensive tutorial on the web with this stuff. SO..here's my two cents.</p>
  </div>
</section>



<style>
          .custom-btn {
                padding: 10px 25px;
                font-family: "Roboto", sans-serif;
                font-weight: 500;
                background: transparent;
                outline: none !important;
                cursor: pointer;
                transition: all 0.3s ease;
                position: relative;
                display: inline-block;
            }
  .btn-3 {
  width: 130px;
  height: 40px;
  line-height: 42px;
  padding: 0;
  border: none;
}
.btn-3 span {
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
}
.btn-3:before,
.btn-3:after {
  position: absolute;
  content: "";
  right: 0;
  top: 0;
  background: #000;
  transition: all 0.3s ease;
}
.btn-3:before {
  height: 0%;
  width: 2px;
}
.btn-3:after {
  width: 0%;
  height: 2px;
}
.btn-3:hover:before {
  height: 100%;
}
.btn-3:hover:after {
  width: 100%;
}
.btn-3 span:before,
.btn-3 span:after {
  position: absolute;
  content: "";
  left: 0;
  bottom: 0;
  background: #000;
  transition: all 0.3s ease;
}
.btn-3 span:before {
  width: 2px;
  height: 0%;
}
.btn-3 span:after {
  width: 0%;
  height: 2px;
}
.btn-3 span:hover:before {
  height: 100%;
}
.btn-3 span:hover:after {
  width: 100%;
}

  </style>
@endsection