<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    
        
    <meta charset="UTF-8">
    <meta name="title" content="ComUp - To The Top">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Ricardo Guerreiro">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Archivo">

    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet" type="text/css">

    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

    <script src="{{ asset('js/jquery.min.js')}}"></script>

    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">


    
    <link rel="icon" href="{{ asset('images/brand/Comup-Favicon.png') }}" type="image/x-icon" />
    <link rel="shortcut icon" href="{{ asset('images/brand/Comup-Favicon.png') }}" type="image/x-icon" />

    <title> Ricardo Guerreiro | Desenvolvimento Web  </title>
</head> 

