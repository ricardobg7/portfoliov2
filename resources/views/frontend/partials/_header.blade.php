<div id='header'>
    <div id="header-title">
        <h1> This a test Title<h1>
    </div>
</div>
<div id='nav'>
    <ul>
        <li> 
            <a href="#">Home</a>
        </li>
        <li> 
            <a href="#">About</a>
        </li>
        <li> 
            <a href="#">Skills</a>
        </li>
        <li> 
            <a href="#">Portfolio</a>
        </li>
        <li> 
            <a href="#">Contacts</a>
        </li>
    </ul>
</div>

<script>
$('#header > h1').addClass('animated fadeInUp');

var nav = $("#nav");
    stickyDiv = "sticky";
    header = $('#header').height();

$(window).scroll(function() {
  if( $(this).scrollTop() > header ) {
    nav.addClass(stickyDiv);
  } else {
    nav.removeClass(stickyDiv);
  }
});
</script>