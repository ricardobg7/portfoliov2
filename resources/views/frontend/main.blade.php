@include('frontend.partials._head')
@include('frontend.partials._header')

<body>    
    <div class="container-fluid">        
        @yield('content')
    </div>
</body>

<script src="{{ asset('js/menu/classie.js') }}" defer></script>
<script src="{{ asset('js/menu/borderMenu.js') }}" defer></script>

@include('frontend.partials._footer')